import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/Dashboard';
import DashboardShop from '@/components/DashboardShop';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/dashboard',
      component: Dashboard,
      children: [
        {
          path: 'shop/new',
          component: DashboardShop,
        },
      ],
    },
  ],
});

FROM node:8.11.3-alpine

WORKDIR /app

RUN apk update && \
    apk add git

CMD ["/bin/sh"]

# picon web
ピコン！とくる情報をビーコンから受け取れるLINE Bot　picon!のwebクライアント  
[https://picon.netlify.com/ ( 利用には登録されたビーコンが必要です )](https://picon.netlify.com/dashboard)

## コンテナのセットアップ、起動、削除
``` bash
# コンテナの作成
docker-compose build

# コンテナの起動
docker-compose up
# localhost:9000で起動できる

# コンテナの削除
docker-compose down
```

## その他のコマンド

``` bash
# ↓コマンドでapp内に入ってからnpm系のコマンドを行う
docker-compose exec app sh

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
